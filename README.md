# yaqc-pydm

[![PyPI](https://img.shields.io/pypi/v/yaqc-pydm)](https://pypi.org/project/yaqc-pydm)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqc-pydm)](https://anaconda.org/conda-forge/yaqc-pydm)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqc-pydm/-/blob/main/CHANGELOG.md)

An interface to the [Python Display Manager](http://slaclab.github.io/pydm/) for [yaq](https://yaq.fyi).
