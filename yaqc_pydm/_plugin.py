__all__ = ["YAQPlugin"]


import pydm  # type: ignore

from ._connection import YAQConnection


class YAQPlugin(pydm.data_plugins.plugin.PyDMPlugin):
    protocol = "yaq"
    connection_class = YAQConnection


pydm.data_plugins.add_plugin(YAQPlugin)
