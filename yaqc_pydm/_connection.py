__all__ = ["YAQConnection"]


from qtpy import QtCore  # type: ignore
import yaqc  # type: ignore
import pydm  # type: ignore


class YAQConnection(pydm.data_plugins.plugin.PyDMConnection):
    def __init__(self, widget, address, protocol=None, parent=None):
        super().__init__(widget, address, protocol, parent)
        self.yaq_host = address.split(":")[0]
        self.yaq_port = int(address.split(":")[1])
        self.yaq_client = yaqc.Client(host=self.yaq_host, port=self.yaq_port)
        self.add_listener(widget)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.send_new_value)
        self.timer.start(1000)
        self.connected = True

    def send_new_value(self):
        measured = self.yaq_client.get_measured()
        self.new_value_signal[str].emit(str(measured))

    def send_connection_state(self, conn):
        self.connection_state_signal.emit(conn)

    def add_listener(self, widget):
        super().add_listener(widget)
        self.send_connection_state(True)
